# **vpk** (Void Package)

VPK (VoidPacKage) is a XBPS wrapper. It handles update, install, remove, search and other operations (see Features). The name "vpk" was chosen because it is easy and fast to type, consists of only 3 characters, and contains "v" for Void.

### Fixes + added features
2023-09-06
- feature: hold (ho), hold+ (ho+), hold- (ho-)
- cleaned up code a little bit
- output simplified and no colors anymore

older fixes:
- fixed: xcheckrestart related stuff
- feature: multiple packages can be chosen to for installation in vpk search | se and vpk search-uninstalled | seu
- fixed error message in some search cases
- fixed install | in issue with multiple packages
- fixed the vdk search-uninstalled | seu issue when choosing a package for install
- fixed the vdk restart | re issue

### Dependencies

- xtools `sudo xbps-install -S xtools` (for the restart (rs) operation)

### How to use

Download:

`git clone https://gitlab.com/Irkiosan/voidlinux-xbps-wrapper.git`

(Optional) Rename the script:

`mv ./voidlinux-xbps-wrapper/vpk ./voidlinux-xbps-wrapper/<YourDesiredName>` Please make sure to replace 'vpk' in the following commands with your chosen name.

Make the scripts executable:

`chmod +x ./voidlinux-xbps-wrapper/vpk`

Move the script to a folder in your $PATH (e.g. /usr/bin):

`sudo mv ./voidlinux-xbps-wrapper/vpk /usr/bin`

Remove the downloaded files:

`rm -rf ./voidlinux-xbps-wrapper/`

Close and re-open the terminal.

### Features

```
Usage: vpk [OPERATION] [PKGNAME...]

OPERATIONS
 install    (in)    Sync repository index, install available PKGs
                    Unavailable PKGNAMEs will be skipped and listed
 remove     (rm)    Remove PKGNAMEs from the system
 search     (se)    Search for PKGNAME in the remote repositories
                    An install option for the listed PKGs is porvided
 search-un  (seu)   Search for PKGNAME in the remote repositories,
                    skip already installed PKGs
                    An install option for the listed PKGs is provided
 hold       (ho)    List packages on hold state
 hold+      (ho+)   Hold PKGNAMEs
 hold-      (ho-)   Unhold PKGNAMEs
 find       (fi)    Show package files for PKGNAME
 info               Show information about PKGNAME
 local      (lo)    List installed packages page-wise
                    Enter PKGNAME to list installed packages for PKGNAME
 update     (up)    Sync repository index and update system
 update+    (up+)   Sync repository index, update system, clean cache,
                    and remove orphans
 cache      (ca)    Clean cache
 orphans    (or)    Remove orphans
 clean      (cl)    Clean cache and remove orphans
 restart    (rs)    System restart suggestion based on xcheckrestart (xtools)
 repos      (re)    List registered repositories
 sync       (sy)    Sync repository index

The vpk script is a wrapper for XBPS.
```
